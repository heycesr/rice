var app = new Vue({
  el: '#app',
  data: {
    score: '0',
    group: {
      number: 0.9
    },
    reach: {
      number: 10
    },
    impact: {
      number: 0.5
    },
    confidence: {
      number: 0
    }
  },
  methods: {
    computeScore: function () {
      this.score = Math.floor( this.group.number * this.reach.number * this.impact.number * this.confidence.number );
    }
  },
  mounted: function () {
    this.computeScore();
  }
})